﻿using Microsoft.EntityFrameworkCore;

namespace Generalnfo.Domain
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=UserDB;Username=test_user;Password=123456789");
        }
    }
}

