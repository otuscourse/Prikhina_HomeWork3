﻿using AutoMapper;

namespace Generalnfo.Model
{
    public class CustomerCreateRequestMapping
    {
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Model.CustomerCreateRequest, CustomerViewModel>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
       
    }
}
