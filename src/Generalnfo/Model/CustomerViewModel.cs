﻿namespace Generalnfo.Model
{
    public class CustomerViewModel
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }
    }
}
