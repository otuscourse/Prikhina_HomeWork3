﻿using AutoMapper;

namespace Generalnfo.Model
{
    public class CustomerMapping
    {
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Customer, CustomerViewModel>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }

        public static Mapper InitializeAutomapperIn()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomerViewModel, Domain.Customer>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
