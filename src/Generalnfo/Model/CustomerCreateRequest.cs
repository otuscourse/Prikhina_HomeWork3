namespace Generalnfo.Model
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest()
        {
        }

        public CustomerCreateRequest(long id, string firstName, string lastName)
        {
            Id = id;
            Firstname = firstName;
            Lastname = lastName;
        }
        public static CustomerCreateRequest RandomCustomer(long id)
        {
            return new CustomerCreateRequest(id, Faker.Name.First(), Faker.Name.Last());
        }

        public long Id { get; set; }

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}