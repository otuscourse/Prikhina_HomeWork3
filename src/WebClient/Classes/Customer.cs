namespace WebClient.Classes
{
    public class Customer
    {
        public long Id { get; init; }

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}