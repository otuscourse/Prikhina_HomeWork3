﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public  class MainMenu
    {
        public static void MenuShow()
        {
            bool getOn = true;
            while (getOn)
            {
                Console.WriteLine("\nВведите:\n" +
                    "AddNewCustomer (add_c) - добавить нового пользователя\n" +
                    "GetCustomer (or get_c) - получить информацию о пользователе\n" +
                    "Finish (or f) - завершение работы\n");
                Console.Write("Ваш ввод: ");

                switch (Console.ReadLine().ToLower())
                {
                    case "addnewcustomer" or "add_c":
                        CustomerService.AddCustomer();
                        break;
                    case "getcustomer" or "get_c":
                        CustomerService.GetCustomer();
                        break;
                    case "finish" or "f":
                        getOn = false;
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Завершение работы");
        }

    }
}
