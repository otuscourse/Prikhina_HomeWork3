﻿using Generalnfo.Model;
using System;
using System.Net.Http;
using System.Net.Http.Json;

namespace WebClient.Services
{
    public class CustomerService
    {
        private static string serverUri = Config.GetServerUri();
        public static void GetCustomer()
        {
            Console.WriteLine("Введите id пользователя: ");
            var id = GeneralService.CheckInputNumber(); 
            string path = serverUri + "Customer/" + id;
            try
            {
                using HttpClient client = new();
                var result = client.GetFromJsonAsync<CustomerViewModel>(path).GetAwaiter().GetResult();
                Console.WriteLine($"Данные пользователя");
                OutCustomer(result);
            }
            catch (HttpRequestException ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    Console.WriteLine($"Пользователем с id={id} не найден");
                else Console.WriteLine(ex.Message);
            }
        }

        public static void AddCustomer()
        {
            Console.WriteLine();
            Console.WriteLine("Введите id пользователя: ");
            var id = GeneralService.CheckInputNumber();
            var addItem = CustomerCreateRequest.RandomCustomer(id);
            var mapper = CustomerCreateRequestMapping.InitializeAutomapperOut();
            var customer = mapper.Map<CustomerViewModel>(addItem);

            string path = serverUri + "Customer";
            using HttpClient client = new();
            try
            {
                var postResponse = client.PostAsJsonAsync(path, customer).GetAwaiter().GetResult();
                postResponse.EnsureSuccessStatusCode();
                var result = postResponse.Content.ReadFromJsonAsync<CustomerViewModel>().GetAwaiter().GetResult();
                Console.WriteLine($"Пользователь успешно добавлен");
                OutCustomer(result);
            }
            catch (HttpRequestException ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.Conflict)
                    Console.WriteLine($"Пользователем с id={id} уже существует");
                else Console.WriteLine(ex.Message);
            }

        }

       

        static void OutCustomer(CustomerViewModel? customer)
        {
            Console.WriteLine($"Id: {customer?.Id}, Lastname: {customer?.Lastname}, Firstname: {customer?.Firstname}");
        }
    }
}
