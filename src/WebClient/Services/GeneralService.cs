﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public class GeneralService
    {
        public static long CheckInputNumber()
        {
            var property = Console.ReadLine();
            var check = long.TryParse(property, out var number);
            while (!check)
            {
                Console.WriteLine($"Введенная строка не является целым числом. Пожалуйста, повторите ввод");
                property = Console.ReadLine();
                check = long.TryParse(property, out number);
            }
            return number;
        }
    }
}
