﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebClient.Services
{

    public class Config
    {
        internal static string GetServerUri()
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false);

                IConfiguration config = builder.Build();
                return config["ServerUri"];
            }
            catch
            {
                return "https://localhost:5001/";
            }
        }
    }

}
