using Generalnfo.Model;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Threading.Tasks;
using WebApi.Service;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : Controller
    {
        [HttpGet("{id:long}")]
        [SwaggerResponse(200, "", typeof(CustomerViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, "������������ �� ������")]
        public async Task<IActionResult> Customer([FromRoute] long id)
        {
            CustomerViewModel output = CustomerService.GetCustomer(id);

            if (output == null)
            {
                return NotFound();
            }
            return Ok(output);
        }

        [HttpPost("")]
        [SwaggerResponse(200, "", typeof(CustomerViewModel))]
        [SwaggerResponse((int)HttpStatusCode.Conflict, "������������ � ����� ID ��� ����������")]
        public async Task<IActionResult> CreateCustomerAsync([FromBody] CustomerViewModel customer)
        {
            CustomerViewModel output = CustomerService.AddCustomer(customer);

            if (output == null)
            {
                return Conflict();
            }

            return Ok(output);
        }
    }
}