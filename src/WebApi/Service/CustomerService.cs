﻿using Generalnfo.Domain;
using Generalnfo.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Service
{
    public class CustomerService
    {
        protected static List<CustomerViewModel> GetAllInfo()
        {
            var customers = new List<Customer>();
            using (var context = new AppDbContext())
            {
                customers = context.Customers.ToList();
            }
            var mapper = CustomerMapping.InitializeAutomapperOut();

            return mapper.Map<List<CustomerViewModel>>(customers);
        }

        public static CustomerViewModel GetCustomer(long id)
        {
            var customer = GetAllInfo().Where(c => c.Id == id).FirstOrDefault();
            return customer;
        }

        public static CustomerViewModel AddCustomer(CustomerViewModel addItem)
        {
            var check = GetAllInfo().Where(c => c.Id == addItem.Id).FirstOrDefault();
            if (check != null)
                return null;
            var mapper = CustomerMapping.InitializeAutomapperIn();
            var item = mapper.Map<Customer>(addItem);
            try
            {
                using (var context = new AppDbContext())
                {
                    context.Customers.Add(item);
                    context.SaveChanges();
                }
                return GetAllInfo().Where(c => c.Id == addItem.Id).FirstOrDefault();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}
